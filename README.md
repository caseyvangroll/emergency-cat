Ideas

Options:
- Add a random cat fact underneath each picture
- Configure what type of picture you want to see...
  - young/old
  - wild/tame
- Make the cat images pull from a web service

-- Vote on next feature...
- What do you think we would need to add this feature...?
- The thinking through of all the different decisions and how you could have creative control over those?

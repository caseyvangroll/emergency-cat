import imageList from './imageList';
// import qs from 'query-string';

// const getImageList = () => {
//   const uri = 'https://api.flickr.com/services/rest/';
//   const query = qs.stringify({
//     api_key: 'a87ab0747dcb9563c5262db46c9a7d7d',
//     method: 'flickr.photos.search',
//     safe_search: 1,
//     content_type: 1,
//     tags: 'cat,cats,face,whiskers,cute',
//     format: 'json',
//     nojsoncallback: 1,
//     sort: 'relevance',
//   });
//   return fetch(`${uri}?${query}`)
//     .then(res => res.json())
//     .then(res => res.photos.photo);
// };

// const pickImageFromList = imageList => {
//   const index = Math.floor(Math.random() * imageList.length);
//   return imageList[index];
// }

export default async function getImageUri() {
  const randomIndex = Math.floor(Math.random() * imageList.length);
  const { filename } = imageList[randomIndex];
  const randomLocalImage = `${process.env.IMAGES_PATH}/cats/${filename}`;
  return randomLocalImage;
  // const imageList = await getImageList();
  // const { id, secret, server } = pickImageFromList(imageList);
  // return `https://live.staticflickr.com/${server}/${id}_${secret}_m.jpg`;
}
